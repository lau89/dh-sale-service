package com.dh.fullstack.sales.salesservicestandalone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Import;

@Import(
        {
                com.dh.fullstack.sales.salesservice.Config.class,
                com.jatun.open.tools.blcmd.Config.class
        })
@EnableEurekaClient
@SpringBootApplication
public class SalesServiceStandaloneApplication {

    public static void main(String[] args) {
        SpringApplication.run(SalesServiceStandaloneApplication.class, args);
    }

}
