package com.dh.fullstack.sales.api.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Lady Cuizara
 */
public interface Client extends Serializable {

    Long getClientId();

    String getEmail();

    String getFirstName();

    String getLastName();

    Boolean getIsDeleted();

    Date getCreatedDate();

    Date getLastPurchase();

}
