package com.dh.fullstack.sales.api.input;

import java.util.Date;

/**
 * @author Lady Cuizara
 */
public class ContactCreateInput {
    private String email;

    private String name;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}