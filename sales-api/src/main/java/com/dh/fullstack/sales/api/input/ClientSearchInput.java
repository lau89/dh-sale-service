package com.dh.fullstack.sales.api.input;

/**
 * @author Lady Cuizara
 */
public class ClientSearchInput {
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
