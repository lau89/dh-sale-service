package com.dh.fullstack.sales.salesservice.model.domain;
import javax.persistence.*;

/**
 * @author Lady Cuizara
 */
@Entity
@Table(name = "detail_table")
public class Detail {
    @Id
    @Column(name = "detailid", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "totalproducts")
    private Integer totalProducts;

    @Column(name = "totalprice")
    private Long totalPrice;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "detailsaleid", referencedColumnName = "saleid", nullable = false)
    private Sale sale;

    public Sale getSale() {
        return sale;
    }

    public void setSale(Sale sale) {
        this.sale = sale;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTotalProducts() {
        return totalProducts;
    }

    public void setTotalProducts(Integer totalProducts) {
        this.totalProducts = totalProducts;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }
}
