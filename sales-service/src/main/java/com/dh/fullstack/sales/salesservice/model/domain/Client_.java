package com.dh.fullstack.sales.salesservice.model.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * @author Lady Cuizara
 */
@StaticMetamodel(Client.class)
public class Client_ {
    public static volatile SingularAttribute<Client, Long> id;

    public static volatile SingularAttribute<Client, String> email;

    public static volatile SingularAttribute<Client, String> firstName;

    public static volatile SingularAttribute<Client, String> lastName;

}
