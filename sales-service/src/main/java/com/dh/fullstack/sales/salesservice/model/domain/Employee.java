package com.dh.fullstack.sales.salesservice.model.domain;

import javax.persistence.*;

/**
 * @author Lady Cuizara
 */

@Entity
@Table(name = "employee_table")
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = "employeeid", referencedColumnName = "personid")
})

public class Employee extends Person {
    @Column(name = "position", length = 50, nullable = false)
    private String position;

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
