package com.dh.fullstack.sales.salesservice.model.repository;

import com.dh.fullstack.sales.salesservice.model.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author Lady Cuizara
 */
public interface ClientRepository extends JpaRepository <Client, Long>, JpaSpecificationExecutor<Client> {
}
