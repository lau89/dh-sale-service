package com.dh.fullstack.sales.salesservice.client.contact.service;

import com.dh.chat.contact.api.input.SystemContactCreateInput;
import com.dh.fullstack.sales.salesservice.client.contact.model.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Lady Cuizara
 */
@Service
public class SystemContactService {

    @Autowired
    private ModuleContactClient client;

    public Contact createContact(SystemContactCreateInput input) {
        return client.createContact(input);
    }
}
