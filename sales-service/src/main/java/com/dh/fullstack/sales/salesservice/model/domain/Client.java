package com.dh.fullstack.sales.salesservice.model.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Lady Cuizara
 */
@Entity
@Table(name = "client_table")
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = "clientid", referencedColumnName = "personid")
})

public class Client extends Person implements Serializable {
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "lastpurchase", nullable = false, updatable =
            false)
    private Date lastPurchase;

    public Date getLastPurchase() {
        return lastPurchase;
    }

    public void setLastPurchase(Date lastPurchase) {
        this.lastPurchase = lastPurchase;
    }
}
