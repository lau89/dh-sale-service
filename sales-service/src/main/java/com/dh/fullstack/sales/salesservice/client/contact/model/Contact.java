package com.dh.fullstack.sales.salesservice.client.contact.model;

import java.util.Date;

/**
 * @author Lady Cuizara
 */
public class Contact implements com.dh.chat.contact.api.model.Contact{

    private Long contactId;

    private Long userId;

    private Long accountId;

    private String email;

    private String name;

    private String avatarId;

    private Date createdDate;

    @Override
    public Long getContactId() {
        return null;
    }

    @Override
    public Long getUserId() {
        return null;
    }

    @Override
    public Long getAccountId() {
        return null;
    }

    @Override
    public String getEmail() {
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getAvatarId() {
        return null;
    }

    @Override
    public Date getCreatedDate() {
        return null;
    }
}
