package com.dh.fullstack.sales.salesservice;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Lady Cuizara
 */
@Configuration
@EnableFeignClients
@ComponentScan("com.dh.fullstack.sales.salesservice")
@EnableAutoConfiguration
public class Config {
}
