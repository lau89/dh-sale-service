package com.dh.fullstack.sales.salesservice.command;

import com.dh.fullstack.sales.api.input.ClientCreateInput;
import com.dh.fullstack.sales.salesservice.model.domain.Client;
import com.dh.fullstack.sales.salesservice.model.repository.ClientRepository;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * @author Lady Cuizara
 */
@SynchronousExecution
public class ClientCreateCmd implements BusinessLogicCommand {
    @Setter
    private Long accountId;

    @Setter
    private ClientCreateInput input;

    @Getter
    private Client client;

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public void execute() {
        client = clientRepository.save(composeClientInstance(input));
    }

    private Client composeClientInstance(ClientCreateInput inputClient) {
        Client client = new Client();
        client.setFirstName(inputClient.getFirstName());
        client.setLastName(inputClient.getLastName());
        client.setEmail(inputClient.getEmail());
        client.setCreatedDate(new Date());
        client.setLastPurchase(inputClient.getLastPurchase());
        client.setDeleted(false);
        return client;
    }

    public Client getClient() {
        return client;
    }

    public void setInput(ClientCreateInput input) {
        this.input = input;
    }

    public Long getAccountId() {

        return accountId;
    }
}
