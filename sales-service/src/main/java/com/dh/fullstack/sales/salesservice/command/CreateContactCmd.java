package com.dh.fullstack.sales.salesservice.command;

import com.dh.chat.contact.api.input.SystemContactCreateInput;
import com.dh.fullstack.sales.api.input.ContactCreateInput;
import com.dh.fullstack.sales.salesservice.client.contact.model.Contact;
import com.dh.fullstack.sales.salesservice.client.contact.service.SystemContactService;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Lady Cuizara
 */
@SynchronousExecution
public class CreateContactCmd implements BusinessLogicCommand {

    @Setter
    private Long userId;
    @Setter
    private Long accountId;
    @Setter
    private ContactCreateInput input;

    @Autowired
    private SystemContactService systemContactService;

    @Getter
    private Contact contactInformation;

    @Override
    public void execute() {

        contactInformation = systemContactService.createContact(createContact(input));
    }

    private SystemContactCreateInput createContact(ContactCreateInput contactCreateInput) {
        SystemContactCreateInput contact = new SystemContactCreateInput();
        contact.setAccountId(accountId);
        contact.setUserId(userId);
        contact.setName(contactCreateInput.getName());
        contact.setEmail(contactCreateInput.getEmail());
        return contact;
    }

}
