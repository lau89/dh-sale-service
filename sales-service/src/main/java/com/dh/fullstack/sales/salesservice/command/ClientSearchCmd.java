package com.dh.fullstack.sales.salesservice.command;

import com.dh.fullstack.sales.api.input.ClientSearchInput;
import com.dh.fullstack.sales.salesservice.model.domain.Client;
import com.dh.fullstack.sales.salesservice.model.domain.Client_;
import com.dh.fullstack.sales.salesservice.model.repository.ClientRepository;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Lady Cuizara
 */@SynchronousExecution
public class ClientSearchCmd implements BusinessLogicCommand {

    @Setter
    private Integer limit;

    @Setter
    private Integer page;

    @Setter
    private ClientSearchInput input;

    @Getter
    List<Client> clients;

    @Getter
    private Integer totalPages;

    @Getter
    private Long totalElements;

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public void execute() {
        clients = new ArrayList<>();

        PageRequest pageRequest = PageRequest.of(page, limit);
        Page<Client> pageResult = clientRepository.findAll(buildSpecification(), pageRequest);


        List<Client> content = pageResult.getContent();
        if (!CollectionUtils.isEmpty(content)) {
            clients.addAll(content);
        }

        totalPages = pageResult.getTotalPages();
        totalElements = pageResult.getTotalElements();

    }

    private Specification<Client> buildSpecification() {
        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            query.orderBy(cb.desc(root.get(Client_.firstName)));
            predicates.add(cb.like(root.get(Client_.firstName), input.getValue()));

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }
}
