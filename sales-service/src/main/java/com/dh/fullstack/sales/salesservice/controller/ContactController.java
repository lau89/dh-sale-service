package com.dh.fullstack.sales.salesservice.controller;

import com.dh.fullstack.sales.api.input.ContactCreateInput;
import com.dh.fullstack.sales.salesservice.client.contact.model.Contact;
import com.dh.fullstack.sales.salesservice.command.CreateContactCmd;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Lady Cuizara
 */
@Api(
        tags = "contact",
        description = "feign client for contact"
)
@RequestMapping("/contact")
@RestController
@RequestScope
public class ContactController {
    @Autowired
    private CreateContactCmd createContactCmd;

    @ApiOperation(
            value = "Create an feign contact"
    )
    @ApiResponses({
            @ApiResponse(
                    code = 401,
                    message = "Unauthorized to create account"
            )
    })
    @RequestMapping(method = RequestMethod.POST)
    public Contact createEmployee(@RequestBody ContactCreateInput input,
                                  @RequestHeader("Account-ID") Long accountId,
                                  @RequestHeader("User-ID") Long userId) {
        createContactCmd.setAccountId(accountId);
        createContactCmd.setUserId(userId);
        createContactCmd.setInput(input);
        createContactCmd.execute();

        return createContactCmd.getContactInformation();
    }
}
