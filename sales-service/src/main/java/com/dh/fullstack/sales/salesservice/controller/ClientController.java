package com.dh.fullstack.sales.salesservice.controller;

import com.dh.fullstack.sales.api.input.ClientCreateInput;
import com.dh.fullstack.sales.api.input.ClientSearchInput;
import com.dh.fullstack.sales.salesservice.command.ClientCreateCmd;
import com.dh.fullstack.sales.salesservice.command.ClientSearchCmd;
import com.dh.fullstack.sales.salesservice.commons.Pagination;
import com.dh.fullstack.sales.salesservice.model.domain.Client;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Lady Cuizara
 */
@Api(
        tags = "client",
        description = "Operations over employees"
)
@RestController
@RequestMapping(value = "public/client")
public class ClientController {
    @Autowired
    private ClientCreateCmd clientCreateCmd;

    @Autowired
    private ClientSearchCmd clientSearchCmd;

    @ApiOperation(
            value = "Create an client"
    )
    @ApiResponses({
            @ApiResponse(
                    code = 401,
                    message = "Unauthorized to create account"
            )
    })
    @RequestMapping(method = RequestMethod.POST)
    public Client createEmployee(@RequestHeader("Account-ID") Long accountId,
                                 @RequestBody ClientCreateInput input) {
        clientCreateCmd.setInput(input);
        clientCreateCmd.execute();

        return clientCreateCmd.getClient();
    }

    @ApiOperation(
            value = "Search Clients"
    )
    @RequestMapping(
            value = "/search",
            method = RequestMethod.POST)
    public Pagination<Client> searchContact(@RequestParam("limit") Integer limit,
                                            @RequestParam("page") Integer page,
                                            @RequestBody ClientSearchInput input) {
        clientSearchCmd.setPage(page);
        clientSearchCmd.setLimit(limit);
        clientSearchCmd.setInput(input);
        clientSearchCmd.execute();

        Pagination<Client> pagination = new Pagination<>();
        pagination.setContent(clientSearchCmd.getClients());
        pagination.setTotalPages(clientSearchCmd.getTotalPages());
        pagination.setTotalElements(clientSearchCmd.getTotalElements());
        return pagination;
    }
}
